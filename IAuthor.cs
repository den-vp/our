﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceDesk
{
    public interface IAuthor
    {
        Task Task { get; set; }

        void AddTask();
    }
}